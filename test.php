<?php

use RdKafka\Conf;
use RdKafka\Consumer;
use RdKafka\Producer;

$options = [
    'metadata.broker.list' => 'kafka:9092',
    'client.id'            => 'user',
];

$config = new Conf();
foreach ($options as $option => $value) {
    $config->set($option, $value);
}

// producer
// --------------
$producer = new Producer($config);
$topic = $producer->newTopic('user');

$body = json_encode([
    'id'         => $id = time(),
    'mail'       => 'someone.' . $id . '@qa.com',
    'first_name' => $id,
    'last_name'  => 'QA',
]);

$topic->produce(RD_KAFKA_PARTITION_UA, 0, $body, "user.create:{$id}");
$producer->flush(10000);

// consumer
// --------------
$consumer = new Consumer();
$consumer->addBrokers('kafka:9092');
$topic = $consumer->newTopic('user');
$topic->consumeStart(0, RD_KAFKA_OFFSET_BEGINNING);
while (true) {
    if (!$message = $topic->consume(0, 5000)) {
        break;
    }

    switch ($message->err) {
        case RD_KAFKA_RESP_ERR_NO_ERROR:
            print_r($message);
            continue 2;
            break;

        case RD_KAFKA_RESP_ERR__PARTITION_EOF:
            break;

        case RD_KAFKA_RESP_ERR__TIMED_OUT:
        default:
            throw new \RuntimeException('Consuming error: ' . $message->errstr());
    }
}
